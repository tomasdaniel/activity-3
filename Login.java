public class Login {
    private User[] users;
    
    public Login(User[] users) {
        this.users = users;
    }
    
    public boolean validate(String username, String password, boolean admin) {
        if (!admin) {
            for (User user : users) {
                if (username.equals(user.getUsername()) && password.equals(user.getPassword())) {
                    return true;
                }
            }
        } else if (admin) {
                for (User user : users) {
                    if (user.getUserType() == UserType.ADMIN &&
                    user.getUsername().equals(username) &&
                     user.getPassword().equals(password)) {
                        return true;
                    }
                }
            }
        return false;
    }
}