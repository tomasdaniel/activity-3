import java.util.Random;
public class passwordGenerator{
    private int length;
    private Random rand;

    public passwordGenerator(int length){
        this.length = length; 
        rand = new Random();
    }
    public String generateWeakPassword(){
        String wPass = "qwertyuiopasdfghjklzxcvbnm";
		StringBuilder ax = new StringBuilder(this.length);
		for (int i = 0; i < this.length; i++)
			ax.append(wPass.charAt(rand.nextInt(wPass.length())));
		return ax.toString();
	}

    public String generateStrongPassword(){
        String sPass = "QWERTYUIOPASDFGHJKLZXCVBNM";
		StringBuilder ax = new StringBuilder(this.length);
		for (int i = 0; i < this.length; i++)
			ax.append(sPass.charAt(rand.nextInt(sPass.length())));
		return ax.toString();
        
    }
}